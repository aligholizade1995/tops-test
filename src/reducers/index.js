import {combineReducers} from 'redux';
import {DefaultReducer} from './DefaultReducer';
import * as action from '../actions/actionTypes';

export const initialize_profile = {
  token: null,
  refresh_token: null,
  user: {
    id: null,
    username: null,
    mobile: null,
    first_name: null,
    last_name: null,
    nik_name: null,
    stage_id: null,
    gender: null,
    address: null,
    postal_code: null,
    birthday: null,
    email: null,
    role: null,
    last_login_at: null,
    last_token_at: null,
    status: true,
    created_at: null,
    updated_at: null
  }
}

const allReducers = combineReducers({
  profile: DefaultReducer(action.PROFILE_RECORD, initialize_profile),
});

export default allReducers;
