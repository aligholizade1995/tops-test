import React from 'react';
import {
  PieChart, Pie, Tooltip,
} from 'recharts';
import './piechart.css';

const data01 = [
  { name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 }, { name: 'Group D', value: 200 },
  { name: 'Group E', value: 278 }, { name: 'Group F', value: 189 },
];

export default class StatisticsPie extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: []
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.data.length !== this.props.data.length) {
      let data = [];
      if (this.props.data && this.props.data.items.length !== 0) {
        for (let item of this.props.data.items) {
          data.push({
            name: item.course_name,
            value: item.count
          })
        }
      }
      this.setState({
        data: data
      })
    }
  }

  render() {
    if (this.state.data.length === 0) {
      return (
        <div className={'pie-temp'}>

        </div>
      )
    }
    return (
      <PieChart className="pie" width={400} height={400}>
        <Pie dataKey="value" isAnimationActive={true} data={this.state.data} cx={200} cy={200} outerRadius={80} fill="#8884d8" label />
        <Tooltip />
      </PieChart>
    );
  }
}
