const width = window.innerWidth;
const height = window.innerHeight;

const BaseWidth = 768;
const BaseHeight = 1024;

const scale = size => (width / BaseWidth) * size;
const verticalScale = size => (height / BaseHeight) * size;
const moderateScale = (size, factor = 0.1) =>
  size + (scale(size) - size) * factor;

export {scale, verticalScale, moderateScale};
