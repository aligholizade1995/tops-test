import React, { PureComponent } from 'react';
import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Legend,
} from 'recharts';
import './barchart.css'

const data = [
  {
    name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
  },
  {
    name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
  },
  {
    name: 'Page C', uv: 2000, pv: 9800, amt: 1000,
  },
  {
    name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
  },
  {
    name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
  },
  {
    name: 'Page F', uv: 2390, pv: 3800, amt: 2500,
  },
  {
    name: 'Page G', uv: 3490, pv: 4300, amt: 2100,
  },
];

export default class Example extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      data: []
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.data.length !== this.props.data.length) {
      let data = [];
      if (this.props.data && this.props.data.items.length !== 0) {
        for (let item of this.props.data.items) {
          data.push({
            name: item.course_name,
            pv: item.count
          })
        }
      }
      this.setState({
        data: data
      })
    }
  }

  render() {
    if (this.state.data.length === 0) {
      return (
        <div className={'bar-temp'}>

        </div>
      )
    }
    return (
      <BarChart
        className="bar"
        width={window.innerWidth > 550 ? 500 : window.innerWidth - 80}
        height={window.innerWidth > 550 ? 300 : 200}
        data={this.state.data}
        margin={{
          top: 5, right: window.innerWidth > 550 ? 30 : 0, left: window.innerWidth > 550 ? 20 : -30, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Legend />
        <Bar dataKey="pv" name={'تعداد'} barSize={20} fill="#8884d8" />
      </BarChart>
    );
  }
}
