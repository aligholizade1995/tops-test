import React from 'react';
import Api from "../../js/Api";
import {connect} from "react-redux";
import Header from "../../components/Header";
import StatisticsPie from "../../components/charts/PieChart";
import StatisticsBar from "../../components/charts/BarChart";
import './home.css';
import QuestionsTable from "../../components/QuestionsTable";
import {Container} from "reactstrap";

const mapStateToProps = state => {
  return {
    profile: state.profile
  }
}

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      questions: [],
      statistics: [],
      courses: [],
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.profile.token === null) {
      this.props.history.push('/');
    }
  }

  componentDidMount() {
    setTimeout(async () => {
      if (this.props.profile.token !== null) {
        this.loadData();
      }
    }, 0)
  }

  async loadData() {
    const questions = await Api.get('/admin/questions', this.props.profile.token);
    const statistics = await Api.get('/admin/questions/stats', this.props.profile.token);
    const courses = await Api.get('/admin/courses', this.props.profile.token);
    console.log('===============');
    console.log(courses);
    this.setState({
      questions: questions,
      statistics: statistics,
      courses: courses,
    })
  }

  render() {
    if (this.props.profile.token === null) {
      return (
        <div>

        </div>
      )
    }
    return (
      <div className="home-container">
        <Header history={this.props.history}/>
          <div className="charts-container">
            <StatisticsPie data={this.state.statistics}/>
            <StatisticsBar data={this.state.statistics}/>
          </div>
        {/*<Container>*/}
        <div className="table-container-c">
        <QuestionsTable data={this.state.questions} courses={this.state.courses}/>
        </div>
        {/*</Container>*/}
      </div>
    )
  }
}

export default connect(mapStateToProps)(Home);
