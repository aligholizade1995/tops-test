export const loginValidation = data => {
  if (data.token) {
    return data
  }
  if (data.validation) {
    const error_data = {
      error: 'validation',
      field: data.field,
      message: data.message
    };
    if (data.field === 'username') {
      console.log('1');
      console.log(error_data);
      return error_data
    }
    if (data.field === 'password') {
      console.log('2');
      console.log(error_data);
      return error_data
    }
  } else if (data.code) {
    console.log('3');
    return {
      error: data.code,
      field: data.field,
      message: data.message
    };
  } else {
    return {
      error: 'unknown'
    }
  }
}
