export default class Api {
  static get(route, token) {
    return this.xhr(route, null, 'GET', token)
  }

  static put(route, params, token) {
    return this.xhr(route, params, 'PUT', token)
  }

  static post(route, params, token) {
    return this.xhr(route, params, "POST", token)
  }

  static delete(route, params, token) {
    return this.xhr(route, params, 'DELETE', token)
  }

  static patch(route, params, token) {
    return this.xhr(route, params, 'PATCH', token)
  }

  static async xhr(route, params, verb, token) {
    const host = 'http://tops.mtamadon.ir/api/v1';
    const url = host + route;
    let headers = {
      'Content-Type': 'application/json',
    };
    if (token !== null) {
      headers['Authorization'] = `Bearer ${token}`;
    }
    let options = Object.assign({method: verb}, params ? {body: JSON.stringify(params)} : null);
    options.headers = headers;
    console.log(options);
    return fetch(url, options)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        return responseJson
      })
      .catch((err) => {
        console.log('errrrrrrrrrrrrrrrrrrr', err)
      })
  }
}
