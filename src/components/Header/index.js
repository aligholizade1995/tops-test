import React from 'react';
import {Nav, NavItem, NavLink, Button} from "reactstrap";
import {Link} from "react-router-dom";
import './header.css'
import {connect} from "react-redux";
import {buildAction} from "../../reducers/DefaultReducer";
import {PROFILE_RECORD} from "../../actions/actionTypes";
import {initialize_profile} from "../../reducers";

const mapStateToProps = state => {
  return {
    profile: state.profile
  }
}

class Header extends React.PureComponent {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this)
  }

  async logout() {
    const action = await buildAction(PROFILE_RECORD);
    this.props.dispatch(action(initialize_profile));
    this.props.history.push('/');
  }

  render() {
    return (
      <div className={'nav-container'}>
        <Nav className={'nav'}>
          <div className="">
            <NavItem className={'nav-item'}>
              <NavLink>
                <Link to={'/home'}>{'صفحه اصلی'}</Link>
              </NavLink>
            </NavItem>
            <NavItem className={'nav-item'}>
              <NavLink>
                <Link to={'/store'}>{'سوال جدید'}</Link>
              </NavLink>
            </NavItem>
          </div>
          <NavItem className={'logout'}>
            <NavLink>
              <Button color={'link'} onClick={this.logout}>{'خروج'}</Button>
            </NavLink>
          </NavItem>
        </Nav>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Header)
