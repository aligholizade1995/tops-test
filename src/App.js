import React from 'react';
import {Route,BrowserRouter as Router,Switch} from 'react-router-dom';
import Login from './scenes/Login';
import Home from './scenes/Home';
import Store from './scenes/Store';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore, compose} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import allReducers from './reducers';

const persistConfig = {
  key: 'root',
  storage: storage,
  stateReconciler: autoMergeLevel2, // see "Merge Process" section for details.
  whitelist: ['profile'],
};
const pReducer = persistReducer(persistConfig, allReducers);

const middleWare = [];

middleWare.push(thunk);

const loggerMiddleware = createLogger({
  predicate: () => process.env.NODE_ENV === 'development',
});
middleWare.push(loggerMiddleware);

const store = createStore(pReducer, compose(applyMiddleware(...middleWare)));

const persistor = persistStore(store);

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path={'/'} component={Login}/>
          <Route exact path={'/home'} component={Home}/>
          <Route exact path={'/store'} component={Store}/>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
