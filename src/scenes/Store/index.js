import React from 'react';
import Header from "../../components/Header";
import {Button, Container, Form, FormGroup, FormText, Input, Label} from "reactstrap";
import Api from "../../js/Api";
import {connect} from "react-redux";
import './store.css'
import Spinner from "reactstrap/es/Spinner";

const mapStateToProps = state => {
  return {
    profile: state.profile,
  }
}

class Store extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      courses: [],
      categories: [],
      validate: false,
      loading: false,
      response: false
    }
    this.getCourses = this.getCourses.bind(this);
    this.changeCategory = this.changeCategory.bind(this);
    this.onChaneLevel = this.onChaneLevel.bind(this);
    this.onChaneQuestion = this.onChaneQuestion.bind(this);
    this.onChaneDescription = this.onChaneDescription.bind(this);
    this.onChaneOption1 = this.onChaneOption1.bind(this);
    this.onChaneOption2 = this.onChaneOption2.bind(this);
    this.onChaneOption3 = this.onChaneOption3.bind(this);
    this.onChaneOption4 = this.onChaneOption4.bind(this);
    this.onChaneCorrectOption = this.onChaneCorrectOption.bind(this);
    this.validateAllInputs = this.validateAllInputs.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.profile.token === null) {
      this.props.history.push('/');
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.getCourses();
    }, 0)
  }

  async getCourses() {
    const courses = await Api.get('/admin/courses', this.props.profile.token);
    this.setState({
      courses: courses
    })
  }

  async changeCourse(e) {
    const selected_course_id = e.target.selectedOptions[0].id;
    const categories = await Api.get('/admin/courses/' + selected_course_id + '/categories', this.props.profile.token);
    this.setState({
      selected_course_id: selected_course_id,
      categories: categories,
    })
  }

  async changeCategory(e) {
    const selected_category_id = e.target.selectedOptions[0].id;
    this.setState({
      selected_category_id: selected_category_id,
    })
  }

  onChaneLevel(e) {
    this.setState({
      level: e.target.value
    })
  }

  onChaneQuestion(e) {
    this.setState({
      question: e.target.value
    })
  }

  onChaneDescription(e) {
    this.setState({
      description: e.target.value
    })
  }

  onChaneOption1(e) {
    this.setState({
      option1: e.target.value
    })
  }

  onChaneOption2(e) {
    this.setState({
      option2: e.target.value
    })
  }

  onChaneOption3(e) {
    this.setState({
      option3: e.target.value
    })
  }

  onChaneOption4(e) {
    this.setState({
      option4: e.target.value
    })
  }

  onChaneCorrectOption(e) {
    this.setState({
      correct_option: e.target.value
    })
  }

  validateAllInputs() {
    const {
      selected_course_id,
      selected_category_id,
      level,
      question,
      description,
      option1,
      option2,
      option3,
      option4,
      correct_option
    } = this.state;
    if (!selected_category_id || !selected_course_id || !level || !question || !description || !option4 || !option3 || !option2 || !option1 || !correct_option) {
      return false
    }
    return !(selected_category_id === 0 || selected_course_id === 0 || level === 0 || question === 0 || description === 0 || option4 === 0 || option3 === 0 || option2 === 0 || option1 === 0 || correct_option === 0);
  }

  async submit() {
    await this.setState({
      validate: false,
    })
    if (!this.validateAllInputs()) {
      await this.setState({
        validate: true
      })
      return
    }
    await this.setState({
      loading: true
    })
    const {
      selected_course_id,
      selected_category_id,
      level,
      question,
      description,
      option1,
      option2,
      option3,
      option4,
      correct_option
    } = this.state;
    const params = {
      course_id: Number(selected_course_id),
      category_id: Number(selected_category_id),
      level: Number(level),
      question: question,
      description: description,
      option1: option1,
      option2: option2,
      option3: option3,
      option4: option4,
      correct_option: Number(correct_option),
      randomize: true,
      book_version: 0,
      for_game: true,
      for_exercise: true,
      has_answer: false
    }
    const data = await Api.post('/admin/questions', params, this.props.profile.token);
    this.setState({
      loading: false,
      response: true,
    })
    setTimeout(() => {
      this.props.history.push('/home')
    }, 1500)
  }

  render() {
    return (
      <div>
        <Header/>
        <Container>
          <div className="container-form">
            <Form className="col-form-label">
              <FormGroup>
                <Label className="float-right">{'درس'}</Label>
                <Input type="select" name="select" id="course" onChange={(e) => this.changeCourse(e)}>
                  {this.state.courses.map(course => <option id={course.id}>{course.title}</option>)}
                </Input>
              </FormGroup>
              <FormGroup disabled={this.state.categories.length === 0}>
                <Label className="float-right">{'فصل'}</Label>
                <Input type="select" name="select" id="category" onChange={(e) => this.changeCategory(e)}>
                  {this.state.categories.map(category => <option id={category.id}>{category.title}</option>)}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label className="float-right">{'سطح سوال'}</Label>
                <Input type="number" id="level" min={1} max={3} onChange={e => this.onChaneLevel(e)}/>
              </FormGroup>
              <FormGroup>
                <Label className="float-right">{'صورت سوال'}</Label>
                <Input type="text" id="question"
                       onChange={e => this.onChaneQuestion(e)}/>
              </FormGroup>
              <FormGroup>
                <Label className="float-right">{'شرح سوال'}</Label>
                <Input type="textarea" id="description" onChange={e => this.onChaneDescription(e)}/>
              </FormGroup>
              <FormGroup>
                <Label className="float-right">{'گزینه ۱'}</Label>
                <Input type="text" id="option1"
                       onChange={e => this.onChaneOption1(e)}/>
              </FormGroup>
              <FormGroup>
                <Label className="float-right">{'گزینه ۲'}</Label>
                <Input type="text" id="option2"
                       onChange={e => this.onChaneOption2(e)}/>
              </FormGroup>
              <FormGroup>
                <Label className="float-right">{'گزینه ۳'}</Label>
                <Input type="text" id="option3"
                       onChange={e => this.onChaneOption3(e)}/>
              </FormGroup>
              <FormGroup>
                <Label className="float-right">{'گزینه ۴'}</Label>
                <Input type="text" id="option4"
                       onChange={e => this.onChaneOption4(e)}/>
              </FormGroup>
              <FormGroup>
                <Label className="float-right">{'گزینه صحیح'}</Label>
                <Input type="number" id="correct-option" min={1} max={4} onChange={e => this.onChaneCorrectOption(e)}/>
              </FormGroup>
              {this.state.validate &&
              <FormGroup className="danger-label">
                <Label className="float-right">{'تمام فیلدها اجباری است'}</Label>
              </FormGroup>
              }
              {this.state.response &&
              <FormGroup>
                <Label className="float-right">{'با موفقیت ذخیره شد'}</Label>
              </FormGroup>
              }

              <Button disabled={this.state.loading}
                      className={['submit-btn ', !this.state.auth_error ? 'validate-auth-space' : '']} color="primary"
                      onClick={() => this.submit()}>
                {this.state.loading &&
                <Spinner className={'submit-spinner'} size={'sm'} color={'light'}/>
                }
                <span className={this.state.loading ? 'submit-loading' : ''}>{'ذخیره'}</span>
              </Button>
              {/*<Button className="float-right" color="primary" onClick={() => this.submit()}>{'ذخیره سوال'}</Button>*/}
            </Form>
          </div>
        </Container>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Store);
