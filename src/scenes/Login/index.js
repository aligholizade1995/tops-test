import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Form, FormGroup, Label, Button} from 'reactstrap';
import Input from "reactstrap/es/Input";
import './login.css';
import Api from "../../js/Api";
import {loginValidation} from "../../js/validator";
import Spinner from "reactstrap/es/Spinner";
import {connect} from "react-redux";
import {buildAction} from "../../reducers/DefaultReducer";
import {PROFILE_RECORD} from "../../actions/actionTypes";

const mapStateToProps = state => {
  return {
    profile: state.profile
  }
}

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      username_danger: false,
      password_danger: false,
      loading: false,
      rememberMe: false
    }
    this.usernameChange = this.usernameChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this);
    this.loginSubmit = this.loginSubmit.bind(this);
    this.rememberMe = this.rememberMe.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.profile.token !== this.props.profile.token) {
      if (this.props.profile.token !== null && this.props.profile.remember) {
        this.props.history.push('/home');
      }
    }
  }

  usernameChange(e) {
    this.setState({
      username: e.target.value
    })
  }

  passwordChange(e) {
    this.setState({
      password: e.target.value
    })
  }

  async loginSubmit() {
    await this.setState({
      loading: true,
      username_danger: false,
      password_danger: false,
      auth_error: false
    })
    const {username, password} = this.state;
    const params = {
      username: username,
      password: password,
    }
    const data = await Api.post('/auth/login', params);
    const validated_data = await loginValidation(data);
    await this.setState({
      loading: false
    })
    if (validated_data.error) {
      if (validated_data.error === 'validation') {
        if (validated_data.field === 'username') {
          console.log(validated_data)
          this.setState({
            username_danger: true,
            username_error: validated_data.message
          })
        }
        if (validated_data.field === 'password') {
          this.setState({
            password_danger: true,
            password_error: validated_data.message
          })
        }
      }
      if (validated_data.error === 'authError') {
        this.setState({
          auth_error: true,
          auth_message: validated_data.message
        })
      }
    } else {
      if (this.state.rememberMe) {
        validated_data['remember'] = true
      }
      const action = await buildAction(PROFILE_RECORD);
      await this.props.dispatch(action(validated_data));
      this.props.history.push('/home');
    }
  }

  rememberMe(e) {
    this.setState({
      rememberMe: true
    })
  }

  render() {
    return (
      <div className={'container-c'}>
        <Container style={{height: window.innerHeight}}>
          <div className="form-container col-lg-4 col-md-5 col-sm-10">
            <div className={'center'}>
              <h2 className={'label'}>{'ورود'}</h2>
            </div>
            <Form>
              <FormGroup className={this.state.username_danger ? '' : 'validate-space'}>
                <Label className={this.state.username_danger ? 'danger-label' : ''}>{'نام کاربری'}</Label>
                <Input className={['input-align', this.state.username_danger ? 'danger-input' : '']} type="text"
                       onChange={this.usernameChange}
                       placeholder="نام کاربری خود را وارد کنید" placeholderTextColor='red'/>
                {this.state.username_danger && !this.state.auth_error &&
                <p className={'form-group-danger'}>{'نام کاربری ' + this.state.username_error}</p>
                }
              </FormGroup>
              <FormGroup className={this.state.password_danger ? '' : 'validate-space'}>
                <Label className={this.state.password_danger ? 'danger-label' : ''}>{'رمز عبور'}</Label>
                <Input className={['input-align', this.state.password_danger ? 'danger-input' : '']} type="password"
                       onChange={this.passwordChange}
                       placeholder="رمز عبور خود را وارد کنید"/>
                {this.state.password_danger && !this.state.auth_error &&
                <p className={'form-group-danger'}>{'رمز عبور ' + this.state.password_error}</p>
                }
              </FormGroup>
              <FormGroup>
                <span className={'remember-me'}>{'مرا به خاطر بسپار'}</span>
                <input value={this.state.rememberMe} onChange={this.rememberMe} type={'checkbox'}/>
              </FormGroup>
              {this.state.auth_error &&
              <p className={'form-group-danger'}>{this.state.auth_message}</p>
              }
              <Button disabled={this.state.loading}
                      className={['submit-btn', !this.state.auth_error ? 'validate-auth-space' : '']} color="primary"
                      onClick={this.loginSubmit}>
                {this.state.loading &&
                <Spinner className={'submit-spinner'} size={'sm'} color={'light'}/>
                }
                <span className={this.state.loading ? 'submit-loading' : ''}>{'ورود'}</span>
              </Button>
            </Form>
          </div>
        </Container>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Login);
