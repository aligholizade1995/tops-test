import React from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from 'reactstrap';
import './table.css'
import Api from "../../js/Api";
import {connect} from "react-redux";

const mapStateToProps = state => {
  return {
    profile: state.profile
  }
}

class QuestionsTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      modal: false,
      modal_data: {},
      category_title: ''
    }
    this.toggle = this.toggle.bind(this);
    this.showDetails = this.showDetails.bind(this);
  }

  async showDetails(item) {
    let category_title = '';
    if (item.category_id !== 0) {
      let categories = await Api.get('/admin/courses/'+item.course_id+'/categories', this.props.profile.token);
      for (let category of categories){
        if (category.id === item.category_id) {
          category_title = category.title
        }
      }
    }
    this.setState({
      modal_data: item,
      modal: !this.state.modal,
      category_title: category_title,
    })
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    })
  }

  convertArrayToObject = (array, key) => {
    const initialValue = {};
    return array.reduce((obj, item) => {
      return {
        ...obj,
        [item[key]]: item,
      };
    }, initialValue);
  };

  render() {
    if (!this.props.data.data) {
      return (
        <div>

        </div>
      )
    }
    const question = this.state.modal_data;
    let courses = [];
    if (this.props.courses && this.props.courses.length !== 0) {
      courses = this.convertArrayToObject(this.props.courses, 'id');
      console.log('==============================================');
      console.log(courses);
    }
    return (
      <Table striped hover>
        <thead>
        <tr>
          <th>{'گزینه صحیح'}</th>
          <th>{'سطح سوال'}</th>
          <th>{'صورت سوال'}</th>
          <th>#</th>
        </tr>
        </thead>
        <tbody>
        {this.props.data && this.props.data.data.map((item, index) =>
          <tr onClick={() => this.showDetails(item)}>
            <td>{item.correct_option}</td>
            <td>{item.level}</td>
            <td>{item.question}</td>
            <th scope="row">{index + 1}</th>
          </tr>
        )}
        </tbody>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={''}>
          <ModalHeader toggle={this.toggle}>{question.question}</ModalHeader>
          <ModalBody>
            <div className="modal-container">
              <Table bordered>
                <tbody className="modal-table">
                <tr scope>
                  <td>{question.question}</td>
                  <th scope="row">{'صورت سوال'}</th>
                </tr>
                <tr>
                  <td>{question.description}</td>
                  <th scope="row">{'شرح سوال'}</th>
                </tr>
                <tr>
                  <td>{question.option1}</td>
                  <th scope="row">{'گزینه ۱'}</th>
                </tr>
                <tr>
                  <td>{question.option2}</td>
                  <th scope="row">{'گزینه ۲'}</th>
                </tr>
                <tr>
                  <td>{question.option3}</td>
                  <th scope="row">{'گزینه ۳'}</th>
                </tr>
                <tr>
                  <td>{question.option4}</td>
                  <th scope="row">{'گزینه ۴'}</th>
                </tr>
                <tr>
                  <td>{question.correct_option}</td>
                  <th scope="row">{'گزینه صحیح'}</th>
                </tr>
                <tr>
                  <td>{question.level}</td>
                  <th scope="row">{'سطح سختی'}</th>
                </tr>
                <tr>
                  <td>{courses.length !== 0 && question.description ? (question.course_id !== 0 ? courses[question.course_id].short_title : null) : null}</td>
                  <th scope="row">{'درس'}</th>
                </tr>
                <tr>
                  <td>{this.state.category_title}</td>
                  <th scope="row">{'فصل'}</th>
                </tr>
                </tbody>
              </Table>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggle}>{'بستن'}</Button>
          </ModalFooter>
        </Modal>
      </Table>
    );
  }
}

export default connect(mapStateToProps)(QuestionsTable);
